import pandas
import numpy as np
import sklearn
from pprint import pprint
import preprocessing
from util import *
from classifiers import *
import warnings

with warnings.catch_warnings():
    warnings.filterwarnings("ignore",category=DeprecationWarning)
    from imblearn.over_sampling import SMOTE
    from imblearn.under_sampling import RandomUnderSampler

path_train_data = '../dataset/KDDTrain+.csv'
path_test_data = '../dataset/KDDTest+.csv'


def main(binary=False, reducedFeature=True):
    X_train, y_train = preprocessing.get_dataset(path_train_data, binary)
    X_test, y_test = preprocessing.get_dataset(path_test_data, binary)

    X_nb, X_test_nb, y_nb = X_train, X_test, y_train  #Dataset for NB because no undersampling needed.

    # Original NSL datasets
    print()
    print('Original NSL Dataset')
    display_class_distribution(y_train, binary)

    # Perform Feature Selection using Variance Thresholding or PCA
    if reducedFeature:
        print('Number of features before Feature Reduction', X_train.shape[1])
        X_train, X_test = preprocessing.feature_reduction(X_train, y_train, X_test, y_test, method='variance')
        X_nb, X_test_nb = X_train, X_test
        print('Number of features after Feature Reduction', X_train.shape[1])



    if not binary:
        # Perform oversampling of U2R using SMOTE
        sm = SMOTE(kind='regular', ratio=0.015)  # ratio of minority class to majority class
        X_train_sm, y_train_sm = sm.fit_sample(X_train, y_train)
        X_nb, y_nb = X_train_sm, y_train_sm  #Apply SMOTE to NB dataset
        # Distribution after SMOTE
        print()
        print('Distribution After SMOTE')
        display_class_distribution(y_train_sm)

        # Perform undersampling of DOS, Normal, Probe and r2l
        rus = RandomUnderSampler()
        X_train_sm_rus, y_train_sm_rus = rus.fit_sample(X_train_sm, y_train_sm)

        # Distribution after Random Undersampling
        print()
        print('Distribution After Random Undersampling')
        display_class_distribution(y_train_sm_rus)

        X_train, y_train = X_train_sm_rus, y_train_sm_rus


    X_train, X_test = preprocessing.feature_normalization(X_train, X_test)
    X_nb, X_test_nb = preprocessing.feature_normalization(X_nb, X_test_nb)
    display_feature_min_max(X_train)
    #cluster_analysis(X_train, y_train, X_test, y_test)
    decisionTree(X_train, y_train, X_test, y_test, binary, reducedFeature)
    randomForest(X_train, y_train, X_test, y_test, binary, reducedFeature)
    neuralNetwork(X_train, y_train, X_test, y_test, binary, reducedFeature)
    naiveBayes(X_nb, y_nb, X_test_nb, y_test, binary)  #Uses NB dataset that is not undersampled
    svm(X_train, y_train, X_test, y_test, binary, reducedFeature)
    clustering(X_train, y_train, X_test, y_test, binary, reducedFeature)

if __name__ == '__main__':
    #print_data_distribution()
    main(True, True)
