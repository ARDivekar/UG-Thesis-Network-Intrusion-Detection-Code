import numpy as np
from preprocessing import get_dataset

path_train_data = '../dataset/KDDTrain+.csv'
path_test_data = '../dataset/KDDTest+.csv'

def subsample(x, y, size=10000, binary=True):
    """
    Subsample the data with the given size. The sampling is without replacement.

    Parameters:
    x : array_like
        Feature matrix
    y : array_like
        Output labels corresponding to the feature matrix
    size : int, default=20000
        The numbers of samples to be taken from the dataset

    Return Value:
    x_small : array_like
        Subsampled feature matrix
    y_small : array_like
        Output labels corresponding to the subsampled feature matrix
    """
    new_counts = np.ceil(np.bincount(y)/y.shape[0] * size)
    data = np.hstack((x, y.reshape((y.shape[0], 1))))
    class_wise_arrays = []
    n_classes = 2 if binary else 5
    for cls in range(n_classes):
        class_wise_arrays.append(data[y == cls])
    
    class_wise_random_indices = []
    for array, count in zip(class_wise_arrays, new_counts):
        class_wise_random_indices.append(np.random.choice(array.shape[0], int(count), replace=False))

    data_small = np.vstack(tuple([array[index, :] for array, index in zip(class_wise_arrays, class_wise_random_indices)]))
    x_small = data_small[:, :-1]
    y_small = data_small[:, -1]
    return x_small, y_small


def display_class_distribution(y_train, binary=False):
    def class_size(cls_id):
        idx = (y_train == cls_id).nonzero()
        return len(idx[0])

    print('total', len(y_train))

    if binary:
        print('attack', class_size(0))
        print('normal', class_size(1))
    else:
        print('dos', class_size(0))
        print('normal', class_size(1))
        print('probe', class_size(2))
        print('r2l', class_size(3))
        print('u2r', class_size(4))

def display_feature_min_max(X):
    num_features = X.shape[1]
    for i in range(0, num_features):
        feature = X[:, i]
        print('Feature', i, ':', 'Min -', np.amin(feature), 'Max -', np.amax(feature), 'Standard Deviation -', feature.std())

def print_data_distribution():
    '''
    Reads the dataset from the path set in path_train_data and path_test_data
    And accordingly displays training and test set class distribution
    '''
    X_train, y_train = get_dataset(path_train_data)
    print('----------TRAIN SET CLASS DISTRIBUTION----------')
    display_class_distribution(y_train)
    X_test, y_test = get_dataset(path_test_data)
    print('----------TEST SET CLASS DISTRIBUTION----------')
    display_class_distribution(y_test)
    print('----------TRAIN SET FEATURE DISTRIBUTION----------')
    display_feature_min_max(X_train)
