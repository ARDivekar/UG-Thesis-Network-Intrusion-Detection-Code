import pandas
import numpy as np
from constants import attack_dict
from sklearn.preprocessing import LabelEncoder, scale, StandardScaler
from sklearn.ensemble import RandomForestClassifier as RF
from sklearn.decomposition import PCA
from sklearn.feature_selection import VarianceThreshold



def get_dataset(filename, binary=False):
    dataframe = pandas.read_csv(filename, names=None)
    array = dataframe.values

    X = array[:, 0:-2]
    y = array[:, -2:-1].ravel()

    X_le, y_le = __string2int_encoding(X, y, binary)
    return X_le, y_le

def __string2int_encoding(X, y, binary=False):
    # label encode categorical string features
    for categorical_idx in [1, 2, 3]:
        le = LabelEncoder()
        X[:, categorical_idx] = le.fit_transform(X[:, categorical_idx])

    m = y.size

    if binary:
        for i in range(0, m):
            if y[i] != 'normal':
                y[i] = 'attack'
    else:
        for i in range(0, m):
            y[i] = attack_dict[y[i]]

    # label encode output classes
    le = LabelEncoder()
    y = le.fit_transform(y)
    #print(le.classes_)
    #scaler = MinMaxScaler()
    #X = scaler.fit_transform(X, y)

    return X, y

def feature_reduction(X_train, y_train, X_test, y_test, method='variance'):
    if method.lower().strip() == 'variance':
        return __variance_method(X_train, X_test)
    elif method.lower().strip() == 'pca':
        return __pca_method(X_train, y_train, X_test, y_test)
    elif method.lower().strip() == 'feature_imp':
        return __feature_importance_method(X_train, y_train, X_test)
    else:
        raise ValueError("Method should be either 'pca' or 'variance'")
        return None

def __pca_method(X_train, y_train, X_test, y_test):
    pca = PCA(n_components=22)
    pca.fit(X_train, y_train)
    X_train = pca.transform(X_train, y_train)
    X_test = pca.transform(X_test, y_test)

    return X_train, X_test

def __variance_method(X_train, X_test):
    var = 0.8
    sel = VarianceThreshold(threshold=(var * (1 - var)))
    sel.fit(X_train)
    idxs = sel.get_support(indices=True)
    X_train = X_train[:, idxs]
    X_test = X_test[:, idxs]

    return X_train, X_test

def __feature_importance_method(X_train, y_train, X_test):
    n_estimators, max_depth = 12, 20
    rf = RF(n_estimators=n_estimators, max_depth=max_depth, n_jobs=-1)
    rf.fit(X_train, y_train)
    importances = rf.feature_importances_
    indices = (importances <= 0.01).nonzero()
    X_train = np.delete(X_train, indices, 1)
    X_test = np.delete(X_test, indices, 1)

    return X_train, X_test


def feature_normalization(X_train, X_test):
    scaler = StandardScaler().fit(X_train)
    return scaler.transform(X_train), scaler.transform(X_test)
