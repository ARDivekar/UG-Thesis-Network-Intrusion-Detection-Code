#!/usr/bin/env python3

import numpy as np
from base import *

X = np.array([])
y = np.array([])
test_data = np.array([])
sampleSize = 0


def __printDistribution(collection, counts, size, showMajor=False, threshold=0.1):
	"""
	Display the distribution based on counts and size

	Parameters:
	showMajor : boolean, default=True
		Display only those items that have atleast 'threshold' percent of data
	threshold : float, percent

	Return Value:
	-
	"""
	if showMajor:
		for item, count in zip(collection, counts):
			if (count/size * 100) > threshold:
				print("{} \t\t --- {}/{} ({:.4}%)".format(item, count, size, count/size * 100))
	else:
		for item, count in zip(collection, counts):
			print("{} \t\t --- {}/{} ({:.4}%)".format(item, count, size, count/size * 100))


def readData():
	"""
	Read the complete dataset.

	Parameters:
	-

	Return Value:
	-
	"""
	global X, y, test_data, sampleSize
	X = readFile('../Dataset/samples')
	y = readFile('../Dataset/labels')
	test_data = readFile('../Dataset/test_data')
	sampleSize = X.shape[0]

readData()


def attackDistribution(showMajor=True):
	"""
	Display attacks distribution in train set

	Parameters:
	showMajor : boolean, default=True
		Display only those items that have atleast 0.1 percent of data

	Return Value:
	-
	"""
	print("\n----------TRAIN SET ATTACK DISTRIBUTION----------")
	attacks, counts = np.unique(y, return_counts=True)
	__printDistribution(attacks, counts, sampleSize, showMajor)


def classDistribution():
	"""
	Display class distribution in train and test set

	Parameters:
	-

	Return Value:
	-
	"""
	print("\n----------TRAIN SET CLASS DISTRIBUTION----------")
	y_ = convertAttacksToClasses(y)
	classes, counts = np.unique(y_, return_counts=True)
	__printDistribution(classes, counts, sampleSize)

	print("\n----------TEST SET CLASS DISTRIBUTION----------")
	test_labels = test_data[:, -1]
	y_ = convertAttacksToClasses(test_labels)
	size = test_labels.shape[0]
	labels, counts = np.unique(y_, return_counts=True)
	__printDistribution(labels, counts, size)


def protocolDistribution():
	"""
	Display protocol distribution in train set

	Parameters:
	-

	Return Value:
	-
	"""
	print("\n----------TRAIN SET PROTOCOL DISTRIBUTION----------")
	protocols, counts = np.unique(X[:, 1], return_counts=True)
	__printDistribution(protocols, counts, sampleSize)


def serviceDistribution(showMajor=True):
	"""
	Display service distribution in train set

	Parameters:
	showMajor : boolean, default=True
		Display only those items that have atleast 0.1 percent of data

	Return Value:
	-
	"""
	print("\n----------TRAIN SET SERVICE DISTRIBUTION----------")
	services, counts = np.unique(X[:, 2], return_counts=True)
	__printDistribution(services, counts, sampleSize, showMajor)


if __name__ == "__main__":

	attackDistribution()
	classDistribution()
	protocolDistribution()
	serviceDistribution()
