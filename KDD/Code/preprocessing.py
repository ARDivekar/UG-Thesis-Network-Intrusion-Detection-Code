#!/usr/bin/env python3

from base import *
import numpy as np
import sklearn.preprocessing as pp


def labelEncode(X, y):
	"""
	Encode Categorical attributes to ints using sklearn.preprocessing.LabelEncoder.
	In this application, features 1,2 and 3 are categorical.

	Parameters:
	X : array_like
		Feature matrix
	y : array_like
		Output labels

	Return Value:
	X_le : array_like
		Label encoded feature matrix
	y_le : array_like
		Label encoded output labels
	"""
	X_le = np.copy(X)
	y_le = np.copy(y)
	le = pp.LabelEncoder()
	for i in range(1,4):
		X_le[:, i] = le.fit_transform(X[:, i])

	y_le = le.fit_transform(y)

	return X_le, y_le


def normalize(X, X_test):
	"""
	Note - Not used
	Feature scaling using Normalization.
	Before scaling we need to reduce the range of features 0,4 and 5 which are as high as 10^10
	(Care has to be taken to scale the test data using the properties of the train data)

	Parameters:
	X : array_like
		Train feature matrix
	X_test : array_like
		Test feature matrix

	Return Value:
	X_norm : array_like
		Normalized train feature matrix
	X_test_norm : array_like
		Normalized test output labels	
	"""
	X_norm = np.copy(X)
	X_test_norm = np.copy(X_test)

	X_norm[:, [0, 4, 5]] = np.log10(1 + X_norm[:, [0, 4, 5]])
	X_test_norm[:, [0, 4, 5]] = np.log10(1 + X_test_norm[:, [0, 4, 5]])
	
	scaler = pp.MinMaxScaler()
	X_norm = scaler.fit_transform(X_norm)
	X_test_norm = scaler.transform(X_test_norm)
	return X_norm, X_test_norm


def standardize(X, X_test):
	"""
	Feature scaling using Standardization.
	(Care has to be taken to scale the test data using the properties of the train data)

	Parameters:
	X : array_like
		Train feature matrix
	X_test : array_like
		Test feature matrix

	Return Value:
	X_norm : array_like
		Normalized train feature matrix
	X_test_norm : array_like
		Normalized test output labels	
	"""
	
	scaler = pp.StandardScaler()
	X_norm = scaler.fit_transform(X)
	X_test_norm = scaler.transform(X_test)
	return X_norm, X_test_norm


def feature_reduction(X, X_test):
	"""
	Drop columns from dataset.
	Unnecessay columns found from DT using feature_importance_. Also corroborated from RFE, RFECV, SelectFromModel, SelectKBest.
	The columns were decided apriori.

	Parameters:
	X : array_like
		Train feature matrix
	X_test : array_like
		Test feature matrix

	Return Value:
	X_reduced : array_like
		Feature Reduced train matrix
	X_test_reduced : array_like
		Feature Reduced test matrix
	"""
	to_be_deleted_cols = [6,8,11,13,14,15,16,17,18,19,20,22,23,24,25,27,30]
	X_reduced = np.delete(X, to_be_deleted_cols, 1)
	X_test_reduced = np.delete(X_test, to_be_deleted_cols, 1)
	return X_reduced, X_test_reduced


if __name__ == "__main__":
	pass