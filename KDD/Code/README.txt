base.py - Utility functions
distribution.py - Run as a script. Displays the various distributions in the data
preprocessing.py - Code for preprocessing the dataset
learn.py - Empty main() function. Better if used in the interpreter. Code for various ML models
treeToRules.py - Displays a decision tree as if-else ladder. Also to be used from the interpreter.
