#!/usr/bin/env python3

import numpy as np


attacksToClassDict = {"apache2":"dos", "back":"dos", "buffer_overflow":"u2r", "ftp_write":"r2l", "guess_passwd":"r2l", "httptunnel":"u2r", "imap":"r2l", "ipsweep":"probe", "land":"dos", "loadmodule":"u2r", "mailbomb":"dos", "mscan":"probe", "multihop":"r2l", "named":"r2l", "neptune":"dos", "nmap":"probe", "perl":"u2r", "phf":"r2l", "pod":"dos", "portsweep":"probe", "processtable":"dos", "ps":"u2r", "rootkit":"u2r", "saint":"probe", "satan":"probe", "sendmail":"r2l", "smurf":"dos", "snmpgetattack":"r2l", "snmpguess":"r2l", "spy":"r2l", "sqlattack":"u2r", "teardrop":"dos", "udpstorm":"dos", "warezclient":"r2l", "warezmaster":"r2l", "worm":"r2l", "xlock":"r2l", "xsnoop":"r2l", "xterm":"u2r", "normal":"normal"}
"""
Mapping for attacks to classes
"""


classDecode = {1:"normal", 2:"probe", 0:"dos", 4:"u2r", 3:"r2l"}
"""
Reverse	mapping of string classes and integer classes
"""


def readFile(file):
	"""
	Reads the given file that has a numpy array.

	Parameters:
	file : string
		File name

	Return Value:
	read numpy array
	"""
	with open(file, 'rb') as inFile:
		return np.load(inFile)


def convertToBinaryLabels(labels):
	y_binary = np.copy(labels)
	y_binary[y_binary != 1] = 0
	return y_binary


def convertAttacksToClasses(y):
	"""
	Converts attacks to attack classes

	Eg: "buffer_overflow" -> "u2r"

	Parameters:
	y : array_like
		Array that has attacks

	Return Value:
	y_ : array_like
		Array, has same shape as y, that has attack classes
	"""
	y_ = np.copy(y)
	for k, v in attacksToClassDict.items():
		y_[y == k] = v
	return y_


def subsample(x, y, size=20000, binary=True):
	"""
	Subsample the data with the given size. The sampling is without replacement.

	Parameters:
	x : array_like
		Feature matrix
	y : array_like
		Output labels corresponding to the feature matrix
	size : int, default=20000
		The numbers of samples to be taken from the dataset

	Return Value:
	x_small : array_like
		Subsampled feature matrix
	y_small : array_like
		Output labels corresponding to the subsampled feature matrix
	"""
	new_counts = np.ceil(np.bincount(y)/y.shape[0] * size)
	data = np.hstack((x, y.reshape((y.shape[0], 1))))
	class_wise_arrays = []
	n_classes = 2 if binary else 5
	for cls in range(n_classes):
		class_wise_arrays.append(data[y == cls])
	
	class_wise_random_indices = []
	for array, count in zip(class_wise_arrays, new_counts):
		class_wise_random_indices.append(np.random.choice(array.shape[0], int(count), replace=False))

	data_small = np.vstack(tuple([array[index, :] for array, index in zip(class_wise_arrays, class_wise_random_indices)]))
	x_small = data_small[:, :-1]
	y_small = data_small[:, -1]
	return x_small, y_small


def clusterToClassMapping(km, y):
	"""
	Map cluster labels into class labels using majority vote.

	Parameters:
	km : MiniBatchKMeans object
		Trained MiniBatchKMeans object
	y : array_like
		Class labels

	Return Value:
	d : dictionary
		Cluster labels as key and corresponding class labels as values
	"""
	d = {}
	for i in range(km.n_clusters):
		labels, counts = np.unique(y[km.labels_ == i], return_counts=True)
		if len(counts) > 0:
			idx = np.argmax(counts)
			d[i] = labels[idx]
		else:
			d[i] = 0
	return d