#!/usr/bin/env python3

from base import *
import numpy as np
from sklearn.tree import DecisionTreeClassifier as DT
from sklearn.ensemble import RandomForestClassifier as RF
from sklearn.neural_network import MLPClassifier as NN
from sklearn.naive_bayes import GaussianNB as NB
from sklearn.svm import SVC as SVM
from sklearn.cluster import MiniBatchKMeans as KM
from sklearn.preprocessing import *
from sklearn.metrics import *
from preprocessing import *
import matplotlib.pyplot as plt
import warnings


X = readFile('../Dataset/samples')  						#Actual train dataset
y_attacks = readFile('../Dataset/labels')  					#Train output attacks as strings
y_string = convertAttacksToClasses(y_attacks)  				#Train output class as strings


test_data = readFile('../Dataset/test_data')
X_test = test_data[:, :-1]  								#Actual test dataset
y_test_string = convertAttacksToClasses(test_data[:, -1])   #Test output class as strings


x, y = labelEncode(X, y_string)  							#Label encode all categorical attributes in train data
x_test, y_test = labelEncode(X_test, y_test_string)  		#Label encode all categorical attributes in test data

x = np.array(x, dtype=np.float64)  							#Type cast all values to float
x_test = np.array(x_test, dtype=np.float64)  				#Type cast all values to float

x_norm, x_test_norm = standardize(x, x_test)  				#Feature scale train and test data. Use standard scaler.

y_binary = convertToBinaryLabels(y)
y_test_binary = convertToBinaryLabels(y_test)

warnings.simplefilter("ignore")  							#Ignore all warnings


def __cluster_analysis(x_train, y_train, x_tst, y_tst, cluster_array=np.arange(100, 1100, 100)):
	"""
	Create many K-Means clustering configurations to find the best K(using elbow method and classfication).
	Plots the graph for elbow method. Also finds accuracy using classification.

	Parameters:
	x_train : array_like
		Train data

	y_train : array_like
		Train labels

	x_tst : array_like
		Test set

	y_tst : array_like
		Test labels

	cluster_array : array_like, default=np.arange(100,1100, 100)
		Array of K values for different K-Means configurations.

	Return Value:
	-
	"""
	clusterers = []
	inertia = []
	scores = []

	print("\n--------------------Cluster Analysis using {} features--------------------\n".format(x_train.shape[1]))

	for n in cluster_array:
		km = KM(n_clusters=n, max_no_improvement=100, init_size=20000, n_init=10, batch_size=10000, reassignment_ratio=0.000001, verbose=False)
		km.fit(x_train)
		clusterers.append(km)
		print("{} clusters done.".format(km.n_clusters))

	print("Calculating inertia and score")
	for km in clusterers:
		inertia.append(km.inertia_)
		scores.append(km.score(x_tst))

	l1, = plt.plot(cluster_array, inertia, label='Inertia')
	l2, = plt.plot(cluster_array, [-s for s in scores], label='Score')
	plt.legend(handles=[l1, l2])
	plt.show()

	print("Cluster to Class mapping started...")
	conversionDicts = []
	for km in clusterers:
		d = clusterToClassMapping(km, y_train)
		conversionDicts.append(d)

	print("Predicting...")
	predictions_array = []
	for i in range(len(clusterers)):
		km = clusterers[i]
		predictions = km.predict(x_tst)
		predictions = [conversionDicts[i][p] for p in predictions]
		predictions_array.append(predictions)
	print("Predicting...Done")

	for predictions, km in zip(predictions_array, clusterers):
		print("\n--------------------{} clusters--------------------\n".format(km.n_clusters))
		print("\nAccuracy : {:.3f}%\n".format(accuracy_score(y_tst, predictions) * 100))
		__displayMetrics(predictions, y_tst)




def __train_and_test(estimator, x_train, y_train, x_tst, y_tst, binary=True):
	"""
	Trains the estimator on the given Train data and test it using the Test data. Also displays class-wise metrics.

	Parameters:
	estimator : estimator object
	(Learning Model)

	x_train : array_like
		Train data

	y : array_like
		Train labels

	x_tst : array_like
		Test set

	y_test : array_like
		Test labels

	binary : boolean, default=True
		True for binary classification between attacks and normal

	Return Value:
	estimator : Returns the trained estimator
	"""
	estimator.fit(x_train, y_train)
	n_features = x_train.shape[1]
	predictions = estimator.predict(x_tst)
	print("\n--------------------{} using {} features--------------------\n".format(estimator.__class__.__name__, n_features))
	print("\nAccuracy : {:.3f}%\n".format(accuracy_score(y_tst, predictions) * 100))
	__displayMetrics(predictions, y_tst, binary)
	return estimator


def __displayMetrics(predictions, y_test, binary=True):
	"""
	Prints the classification report and the confusion matrix

	Parameters:
	predictions : array_like
		Prediction made by the model for the test data

	y_test : array_like
		Test labels

	binary : boolean, default=True
		True for binary classification between attacks and normal

	Return Value:
	-
	"""
	if binary:
		classes = ["attack", "normal"]
	else:
		classes = ["dos", "normal", "probe", "r2l", "u2r"]
	print("\nClass-wise Performance Metrics : ")
	print(classification_report(y_test, predictions, target_names=classes))
	print("\nConfusion Matrix : \n")
	print(confusion_matrix(y_test, predictions), end="\n\n")


def decisionTree(binary=True, reducedFeature=True):
	"""
	Constructs a Decision Tree classifier, trains and tests it.
	The tree is constructed with the best parameters possible.

	Parameters:
	binary : boolean, default=True
		If true, perform binary classification between attacks and normal

	reducedFeature : boolean, default=True
		Use the reduced feature set

	Return Value:
	dt : trained Decision Tree object
	"""
	x_train, x_tst = feature_reduction(x_norm, x_test_norm) if reducedFeature else (x_norm, x_test_norm)
	max_depth = 20 if reducedFeature else None
	y_train, y_tst = (y_binary, y_test_binary) if binary else (y, y_test)
	dt = DT(max_depth=max_depth)
	dt = __train_and_test(dt, x_train, y_train, x_tst, y_tst, binary)
	print("Splitting Criterion : {}".format(dt.criterion))
	print("Depth of Tree : {}".format(dt.tree_.max_depth))
	return dt


def randomForest(binary=True, reducedFeature=True):
	"""
	Constructs a Random Forest, trains and tests it.
	The Random Forest is constructed with the best parameters possible.

	Parameters:
	binary : boolean, default=True
		If true, perform binary classification between attacks and normal

	reducedFeature : boolean, default=True
		Use the reduced feature set

	Return Value:
	rf : trained Random Forest object
	"""
	x_train, x_tst = feature_reduction(x_norm, x_test_norm) if reducedFeature else (x_norm, x_test_norm)
	n_estimators, max_depth = (12, 20) if reducedFeature else (13, None)
	y_train, y_tst = (y_binary, y_test_binary) if binary else (y, y_test)
	rf = RF(n_estimators=n_estimators, max_depth=max_depth, n_jobs=-1)
	rf = __train_and_test(rf, x_train, y_train, x_tst, y_tst, binary)
	print("Splitting Criterion : {}".format(rf.criterion))
	print("Depth of {} Decision Trees : {} ".format(rf.n_estimators, [estimator.tree_.max_depth for estimator in rf.estimators_]))
	return rf



def neuralNetwork(binary=True, reducedFeature=True):
	"""
	Constructs a Neural Network, trains and tests it.
	The Neural Network is constructed with the best parameters possible.

	Parameters:
	binary : boolean, default=True
		If true, perform binary classification between attacks and normal

	reducedFeature : boolean, default=True
		Use the reduced feature set

	Return Value:
	nn : trained Neural Network object
	"""
	x_train, x_tst = feature_reduction(x_norm, x_test_norm) if reducedFeature else (x_norm, x_test_norm)
	hls, alpha, early_stopping = ((20,), 0.00001, False) if reducedFeature else ((30,), 0.0001, True)
	y_train, y_tst = (y_binary, y_test_binary) if binary else (y, y_test)
	nn = NN(hidden_layer_sizes=hls, alpha=alpha, early_stopping=early_stopping)
	nn = __train_and_test(nn, x_train, y_train, x_tst, y_tst, binary)
	print("No. of Hidden Layers: {}".format(len(nn.hidden_layer_sizes)))
	print("Each Hidden layer size : {}".format(", ".join([str(size) for size in nn.hidden_layer_sizes])))
	print("Activation function : {}".format(nn.activation))
	return nn


def naiveBayes(binary=True, reducedFeature=True):
	"""
	Constructs a Naive Bayes classifier, trains and tests it.
	Priors are calculated from the data.

	Parameters:
	binary : boolean, default=True
		If true, perform binary classification between attacks and normal

	reducedFeature : boolean, default=True
		Use the reduced feature set

	Return Value:
	nb : trained Naive Bayes classifier object
	"""
	x_train, x_tst = feature_reduction(x_norm, x_test_norm) if reducedFeature else (x_norm, x_test_norm)
	y_train, y_tst = (y_binary, y_test_binary) if binary else (y, y_test)
	nb = NB()
	nb = __train_and_test(nb, x_train, y_train, x_tst, y_tst, binary)
	return nb


def svm(binary=True, reducedFeature=True, size=20000, cache=1000):
	"""
	Constructs a SVM classifier, trains and tests it.
	The SVM is constructed with the best parameters possible maximizing hardware utilization.

	The data is subsampled randomly to train and test as it is not practical to use more than a couple of 10000 samples for SVM with RBF kernel.

	Parameters:
	binary : boolean, default=True
		If true, perform binary classification between attacks and normal

	reducedFeature : boolean, default=True
		Use the reduced feature set

	size : int/long, default=20000
		The number of train and test examples to be sampled from the complete dataset.

	cache : float, default=1000
		Size of kernel cache in MB (usually from RAM)

	Return Value:
	svm : trained SVM classifier object
	"""
	x_train, x_tst = feature_reduction(x_norm, x_test_norm) if reducedFeature else (x_norm, x_test_norm)
	y_train, y_tst = (y_binary, y_test_binary) if binary else (y, y_test)
	x_small, y_small = subsample(x_train, y_train, size, binary)
	x_test_small, y_test_small = subsample(x_tst, y_tst, size, binary)
	svm = SVM(C=10, cache_size=cache, class_weight='balanced')		#Found the value of C by trial and error.
	svm = __train_and_test(svm, x_small, y_small, x_test_small, y_test_small, binary)
	print("Kernel : {}".format(svm.kernel))
	print("No. of training samples : {}".format(x_small.shape[0]))
	print("No. of Support vectors : {}".format(svm.support_.shape[0]))
	return svm


def clustering(binary=True, reducedFeature=True):
	"""
	Constructs a K-Means Clustering configuration, trains and tests it.
	The configuration is constructed with the best parameters possible.

	Parameters:
	binary : boolean, default=True
		If true, perform binary classification between attacks and normal

	reducedFeature : boolean, default=True
		Use the reduced feature set

	Return Value:
	km : trained K-Means Clustering object
	"""
	x_train, x_tst = feature_reduction(x_norm, x_test_norm) if reducedFeature else (x_norm, x_test_norm)
	y_train, y_tst = (y_binary, y_test_binary) if binary else (y, y_test)
	clusters = 300 if reducedFeature else 400
	km = KM(n_clusters=clusters, max_no_improvement=100, init_size=20000, n_init=10, batch_size=10000, reassignment_ratio=0.000001)
	km.fit(x_train)
	clusterToClass = clusterToClassMapping(km, y_train)
	predictions = km.predict(x_tst)
	predictions = [clusterToClass[p] for p in predictions]
	print("\n--------------------{} using {} features--------------------\n".format(km.__class__.__name__, x_train.shape[1]))
	print("\nAccuracy : {:.3f}%\n".format(accuracy_score(y_tst, predictions) * 100))
	__displayMetrics(predictions, y_tst, binary)
	print("No. of clusters : {}".format(km.n_clusters))
	return km


if __name__ == "__main__":
	pass
