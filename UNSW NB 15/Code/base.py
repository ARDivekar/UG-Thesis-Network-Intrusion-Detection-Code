#!/usr/bin/env python3

import numpy as np

columns = np.array(['id', 'dur', 'proto', 'service', 'state', 'spkts', 'dpkts', 'sbytes', 'dbytes', 'rate', 'sttl', 'dttl', 'sload','dload', 'sloss', 'dloss', 'sinpkt', 'dinpkt', 'sjit', 'djit', 'swin', 'stcpb', 'dtcpb', 'dwin', 'tcprtt', 'synack','ackdat', 'smean', 'dmean', 'trans_depth', 'response_body_len', 'ct_srv_src', 'ct_state_ttl', 'ct_dst_ltm', 'ct_src_dport_ltm', 'ct_dst_sport_ltm', 'ct_dst_src_ltm', 'is_ftp_login', 'ct_ftp_cmd', 'ct_flw_http_mthd', 'ct_src_ltm', 'ct_srv_dst', 'is_sm_ips_ports', 'attack_cat', 'label'])
"""
Column names in the complete dataset (id is removed and attack_cat and label are used as outputs) (proto, service, state, attack_cat) are the categorical attributes
"""


def readFile(file):
	"""
	Reads the given file that has a numpy array.

	Parameters:
	file : string
		File name

	Return Value:
	read numpy array
	"""
	with open(file, 'rb') as inFile:
		return np.load(inFile)


def subsample(x, y, size=25000, binary=True):
	"""
	Subsample the data with the given size. The sampling is without replacement.

	Parameters:
	x : array_like
		Feature matrix
	y : array_like
		Output labels corresponding to the feature matrix
	size : int, default=20000
		The numbers of samples to be taken from the dataset

	Return Value:
	x_small : array_like
		Subsampled feature matrix
	y_small : array_like
		Output labels corresponding to the subsampled feature matrix
	"""
	new_counts = np.ceil(np.bincount(y)/y.shape[0] * size)
	data = np.hstack((x, y.reshape((y.shape[0], 1))))
	class_wise_arrays = []
	n_classes = 2 if binary else 10
	for cls in range(n_classes):
		class_wise_arrays.append(data[y == cls])
	
	class_wise_random_indices = []
	for array, count in zip(class_wise_arrays, new_counts):
		class_wise_random_indices.append(np.random.choice(array.shape[0], int(count), replace=False))

	data_small = np.vstack(tuple([array[index, :] for array, index in zip(class_wise_arrays, class_wise_random_indices)]))
	x_small = data_small[:, :-1]
	y_small = data_small[:, -1]
	return x_small, y_small


def clusterToClassMapping(km, y):
	"""
	Map cluster labels into class labels using majority vote.

	Parameters:
	km : MiniBatchKMeans object
		Trained MiniBatchKMeans object
	y : array_like
		Class labels

	Return Value:
	d : dictionary
		Cluster labels as key and corresponding class labels as values
	"""
	d = {}
	for i in range(km.n_clusters):
		labels, counts = np.unique(y[km.labels_ == i], return_counts=True)
		if len(counts) > 0:
			idx = np.argmax(counts)
			d[i] = labels[idx]
		else:
			d[i] = 0
	return d