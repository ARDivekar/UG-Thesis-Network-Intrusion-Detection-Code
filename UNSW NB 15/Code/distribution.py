#!/usr/bin/env python3

import numpy as np
from base import *

X = np.array([])  # Complete feature matrix(43 features)
y = np.array([])  # 2 dimensional (1st column->Attack name, 2nd column->0/1 for normal/attack)
test_data = np.array([])
sampleSize = 0


class_int2str = {0:"Normal", 1:"Attack"}


def __printDistribution(collection, counts, size, showMajor=False, threshold=0.1):
	"""
	Display the distribution based on counts and size

	Parameters:
	showMajor : boolean, default=True
		Display only those items that have atleast 'threshold' percent of data
	threshold : float, percent

	Return Value:
	-
	"""
	if showMajor:
		for item, count in zip(collection, counts):
			if (count/size * 100) > threshold:
				print("{} \t\t --- {}/{} ({:.4}%)".format(item, count, size, count/size * 100))
	else:
		for item, count in zip(collection, counts):
			print("{} \t\t --- {}/{} ({:.4}%)".format(item, count, size, count/size * 100))


def readData():
	"""
	Read the complete dataset.

	Parameters:
	-

	Return Value:
	-
	"""
	global X, y, test_data, sampleSize
	X = readFile('../Dataset/samples')
	y = readFile('../Dataset/labels')
	test_data = readFile('../Dataset/test_data')
	sampleSize = X.shape[0]

readData()


def attackDistribution(showMajor=False):
	"""
	Display attacks distribution in train set

	Parameters:
	showMajor : boolean, default=True
		Display only those items that have atleast 0.1 percent of data

	Return Value:
	-
	"""
	print("\n----------TRAIN SET ATTACK DISTRIBUTION----------")
	attacks, counts = np.unique(y[:, 0], return_counts=True)
	__printDistribution(attacks, counts, sampleSize, showMajor)

	print("\n----------TEST SET ATTACK DISTRIBUTION----------")
	test_labels = test_data[:, -2]
	size = test_labels.shape[0]
	test_attacks, counts = np.unique(test_labels, return_counts=True)
	__printDistribution(test_attacks, counts, size, showMajor)


def classDistribution():
	"""
	Display class distribution in train and test set

	Parameters:
	-

	Return Value:
	-
	"""
	print("\n----------TRAIN SET CLASS DISTRIBUTION----------")
	classes, counts = np.unique(y[:, 1], return_counts=True)
	__printDistribution([class_int2str[cls] for cls in classes], counts, sampleSize)

	print("\n----------TEST SET CLASS DISTRIBUTION----------")
	test_labels = test_data[:, -1]
	size = test_labels.shape[0]
	labels, counts = np.unique(test_labels, return_counts=True)
	__printDistribution([class_int2str[label] for label in labels], counts, size)


def protocolDistribution(showMajor=True):
	"""
	Display protocol distribution in train set

	Parameters:
	showMajor : boolean, default=True
		Display only those items that have atleast 0.1 percent of data

	Return Value:
	-
	"""
	print("\n----------TRAIN SET PROTOCOL DISTRIBUTION----------")
	protocols, counts = np.unique(X[:, 2], return_counts=True)
	__printDistribution(protocols, counts, sampleSize, showMajor)


def serviceDistribution(showMajor=False):
	"""
	Display service distribution in train set

	Parameters:
	showMajor : boolean, default=True
		Display only those items that have atleast 0.1 percent of data

	Return Value:
	-
	"""
	print("\n----------TRAIN SET SERVICE DISTRIBUTION----------")
	services, counts = np.unique(X[:, 3], return_counts=True)
	__printDistribution(services, counts, sampleSize, showMajor)


if __name__ == "__main__":

	attackDistribution()
	classDistribution()
	protocolDistribution()
	serviceDistribution()
