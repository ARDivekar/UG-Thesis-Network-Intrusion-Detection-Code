import numpy as np
import matplotlib.pyplot as plt
plt.ion()


def alpha_f1(f1_and_supports, alpha, printing=True, print_intermediate=True):
    N = len(f1_and_supports)
    f1_and_supports = sorted(f1_and_supports, key=lambda x: x[1], reverse=True)
    total_support = sum([f1_and_support[1] for f1_and_support in f1_and_supports])
    val = float(0)
    if printing:
        print('\n' + '-' * 50)
        print('[(f1, support)] = %s' % f1_and_supports)
        print('N=%s, alpha=%s' % (N, alpha))
        if print_intermediate:
            print('\t val = 0')
    contributions = []
    for f1_and_support in f1_and_supports:
        f1 = f1_and_support[0]
        support = f1_and_support[1]
        val += f1 * (1 - (alpha * support / total_support))
        contributions.append( (support, 1 - (alpha * support / total_support) ))
        if printing and print_intermediate:
            print('\t     + %s*(1 - (%s*%s)) \t= %s*(1 - %s) \t= %s*%s \t= %s' % (f1, alpha, support / total_support,  f1, alpha *
                                                                                  support / total_support, f1, 1 - (alpha * support / total_support), f1 * (1 - alpha * support / total_support)))
    if printing and print_intermediate:
        print('\n\t     = %s' % (val))
        print('\n\tDividing by %s:\n' % (N - alpha))
    val = val / float(N - alpha)
    if printing:
        print('\t val = %s' % (val))

    total_contrib = sum([contrib[1] for contrib in contributions])
    for contrib in contributions:
        print("\tNormalized contribution of class with %10.5f examples: %3.3f"%(contrib[0], contrib[1]/total_contrib))
    return val

f1_and_supports = [(60, 75.61), (100, 23), (100, 1.289), (100, 0.093), (100, 0.0048)]
N = len(f1_and_supports)
step = 0.01
alpha_range = np.arange(-10, 1+step, step)
val_range = []
for a in alpha_range:
    if round(a, 3) != N:
        val_range.append(
            alpha_f1(f1_and_supports=f1_and_supports, alpha=a,
                     printing=True, print_intermediate=False)
        )
    else:
        val_range.append(val_range[-1])


fig = plt.subplot(1, 1, 1)
fig.plot([alpha_range[0], alpha_range[-1]], [100, 100], color='k')
fig.plot([alpha_range[0], alpha_range[-1]], [0, 0], color='k')
fig.set_xlabel('Alpha')
fig.set_ylabel('F1-Alpha score')
fig.plot(alpha_range, val_range, color='g')
# fig.scatter(alpha_range, val_range, color='g')
input("\nPress Enter to continue:\n > ")
